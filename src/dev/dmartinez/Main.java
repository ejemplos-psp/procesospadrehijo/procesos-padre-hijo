package dev.dmartinez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Main {


	public static void main(String[] args) throws IOException {
		Logger logger = Logger.getLogger(Main.class.getName());
		logger.setLevel(Level.INFO);

		ProcessBuilder processBuilder = new ProcessBuilder("java", "-jar", "hijo.jar");
		processBuilder.redirectErrorStream(true);


		Scanner scanner = new Scanner(System.in);
		String line;

		try {
			Process process = processBuilder.start();
			PrintWriter printWriter = new PrintWriter(process.getOutputStream());
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			System.out.print("Insert any text (Father): ");
			line = scanner.nextLine();

			printWriter.println(line);
			printWriter.flush();

			line = bufferedReader.readLine();
			System.out.println(line);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
